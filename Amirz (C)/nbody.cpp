#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <time.h>

//Amir Najafgholi
// Version 10.0

int n,stpn ;
float m;
//#define G 6.67

using namespace std;

void a(float x , float y , float z ,float* xprim , float* yprim , float* zprim , float* chm , float* input , int f ){
    extern int n,stpn;
    extern float m;
    float r , xzegond , yzegond , zzegond ,cx , cy , cz ;
    float aprim;
    int i,bidbid;
    aprim = 0.0;
    input[0] = 0.0;
    input[1] = 0.0;
    input[2] = 0.0;
    for( i=0 ; i<n ; i++ ){
        xzegond = x - xprim[i];
        yzegond = y - yprim[i];
        zzegond = z - zprim[i];
        r = sqrt( xzegond*xzegond + yzegond*yzegond + zzegond*zzegond );
        cx = (xzegond / r);
        cy = (yzegond / r);
        cz = (zzegond / r);
        bidbid = 1;
        if ( (r > 0.1f) ){
            aprim = bidbid*chm[i]/( r*r );
            input[0] = bidbid*aprim*cx + input[0];
            input[1] = bidbid*aprim*cy + input[1];
            input[2] = bidbid*aprim*cz + input[2];
        }
    }
}

int main( )
{
    const float G = 1;
    FILE *pFile1 , *pFile2;
    //FILE *ifp;
    extern int n,stpn;
    extern float m;
    int  i,k,j,kk;
    float dt;
    float mass;
    float bb;
    pFile1 = fopen ("Output1.dat","w");
    pFile2 = fopen ("Output2.dat","w");
    printf("Number of Bodies,Steps,Time Interval,Mass Unit,Snapshot Time Intervals\n");
    cin>> n >> stpn >> dt >> mass>> kk;
    size_t dim = n * sizeof(float);
    float* acc  = (float*)malloc( 3*sizeof(float) );
    float* x    = (float*)malloc(dim);  float* y    = (float*)malloc(dim);  float* z    = (float*)malloc(dim);
    float* xp   = (float*)malloc(dim);  float* yp   = (float*)malloc(dim);  float* zp   = (float*)malloc(dim);
    float* vx   = (float*)malloc(dim);  float* vy   = (float*)malloc(dim);  float* vz   = (float*)malloc(dim);
    float* kx1  = (float*)malloc(dim);  float* ky1  = (float*)malloc(dim);  float* kz1  = (float*)malloc(dim);
    float* kx2  = (float*)malloc(dim);  float* ky2  = (float*)malloc(dim);  float* kz2  = (float*)malloc(dim);
    float* kx3  = (float*)malloc(dim);  float* ky3  = (float*)malloc(dim);  float* kz3  = (float*)malloc(dim);
    float* kx4  = (float*)malloc(dim);  float* ky4  = (float*)malloc(dim);  float* kz4  = (float*)malloc(dim);
    float* kvx1 = (float*)malloc(dim);  float* kvy1 = (float*)malloc(dim);  float* kvz1 = (float*)malloc(dim);
    float* kvx2 = (float*)malloc(dim);  float* kvy2 = (float*)malloc(dim);  float* kvz2 = (float*)malloc(dim);
    float* kvx3 = (float*)malloc(dim);  float* kvy3 = (float*)malloc(dim);  float* kvz3 = (float*)malloc(dim);
    float* kvx4 = (float*)malloc(dim);  float* kvy4 = (float*)malloc(dim);  float* kvz4 = (float*)malloc(dim);
    float* chm  = (float*)malloc(dim);
    float  c1;
    time_t t1 = clock();
    m=-1*G * mass;

//  ifp = fopen ("Input.dat","r");
//  for (i=0 ; i<n ; i++ ){
//      fscanf (ifp, "%f	%f	%f	%f	%f	%f	%f	" ,&c1, &x[i], &y[i],&z[i], &vx[i], &vy[i], &vz[i]);
//      chm[i]=c1*m;
//  }
//  for ( i=0; i<n ; i++ ){
//      x[i]  = rand() % 10;
//      y[i]  = rand() % 10;
//      z[i]  = rand() % 10;
//      vx[i] = rand() % 100*.01;
//      vy[i] = rand() % 100*.01;
//      vz[i] = rand() % 100*.01;
//  }

    for ( i=0; i<n ; i++ ){
        x[i]  =  i+1;
        y[i]  =  i+1;
        z[i]  =  i+1;
        vx[i] = i*2*pow(-1,i);
        vy[i] = i*2*pow(-1,i);
        vz[i] = i*2*pow(-1,i);
        chm[i] = 0.01*m;
    }

//  x[0] = 0.0    ;
//  y[0] = 0.0    ;
//  z[0] = 0.0    ;
//  vx[0] = 0.0    ;
//  vy[0] = 0.0    ;
//  vz[0] = 0.0    ;
//  chm[0] = 1000*m ;
//
//  x[1] = 10.0   ;
//  y[1] = 0.0    ;
//  z[1] = 0.0    ;
//  vx[1] = 0.0    ;
//  vy[1] = 10.0   ;
//  vz[1] = 0.0    ;
//  chm[1] = 0.01*m ;
//
//  x[2]  = -20.0  ;
//  y[2]  =  0.0   ;
//  z[2]  =  0.0   ;
//  vx[2]  =  0.0   ;
//  vy[2]  = -2.5   ;
//  vz[2]  =  0.0   ;
//  chm[2]  =  0.01*m;

    for( k=0 ; k<=stpn ; k++){
//      printf ("%d ", k);
//      printf ("\n");
//      if (k%kk == 0.0) {
//          for( j=0 ; j<n ; j++  ){
//              fprintf (pFile1, "%f	%f	%f	" ,x[j],y[j],z[j]);
//              fprintf (pFile2, "%f	%f	%f	" ,vx[j],vy[j],vz[j]);
//          }
//          fprintf (pFile1, "\n");
//          fprintf (pFile2, "\n");
        time_t t1a = clock();
        for( i=0 ; i < n ; i++) {
            a( x[i] , y[i] , z[i] , x , y , z, chm , acc , i );
            kvx1[i] = acc[0];
            kvy1[i] = acc[1];
            kvz1[i] = acc[2];
            kx1[i]  = vx[i];
            ky1[i]  = vy[i];
            kz1[i]  = vz[i];
        }
        time_t t2a = clock();
        //printf("Time(farakhooni): %f\n",(t2a-t1a)/(float)CLOCKS_PER_SEC);
        for( i=0 ; i < n ; i++) {
            xp[i] =  x[i] + dt*0.50 * kx1[i] ;
            yp[i] =  y[i] + dt*0.50 * ky1[i] ;
            zp[i] =  z[i] + dt*0.50 * kz1[i] ;
        }
        for( i=0 ; i < n ; i++) {
            a( xp[i]  , yp[i]  , zp[i] ,xp , yp , zp, chm , acc, i );
            kvx2[i] = acc[0];
            kvy2[i] = acc[1];
            kvz2[i] = acc[2];
        }
        for( i=0 ; i < n ; i++) {
            kx2[i]  = vx[i] + (kvx1[i] * dt*0.50) ;
            ky2[i]  = vy[i] + (kvy1[i] * dt*0.50) ;
            kz2[i]  = vz[i] + (kvz1[i] * dt*0.50) ;
        }
        for( i=0 ; i < n ; i++) {
            xp[i] =  x[i] + dt*0.50 * kx2[i] ;
            yp[i] =  y[i] + dt*0.50 * ky2[i] ;
            zp[i] =  z[i] + dt*0.50 * kz2[i] ;
        }
        for( i=0 ; i < n ; i++) {
            a( xp[i]  , yp[i]  , zp[i] , xp , yp , zp,chm , acc, i );
            kvx3[i] = acc[0];
            kvy3[i] = acc[1];
            kvz3[i] = acc[2];
        }
        for( i=0 ; i < n ; i++) {
            kx3[i]  = vx[i] + (kvx2[i] * dt * 0.50) ;
            ky3[i]  = vy[i] + (kvy2[i] * dt * 0.50) ;
            kz3[i]  = vz[i] + (kvz2[i] * dt * 0.50 );
        }
        for( i=0 ; i < n ; i++) {
            xp[i] =  xp[i] + dt*0.50 * kx3[i] ;
            yp[i] =  yp[i] + dt*0.50 * ky3[i] ;
            zp[i] =  zp[i] + dt*0.50 * kz3[i] ;
        }
        for( i=0 ; i < n ; i++) {
            a(xp[i]  , yp[i] , zp[i]  , xp , yp , zp , chm,acc , i );
            kvx4[i] = acc[0];
            kvy4[i] = acc[1];
            kvz4[i] = acc[2];
        }
        for( i=0 ; i < n ; i++) {
            kx4[i]  = vx[i] + (kvx3[i] * dt) ;
            ky4[i]  = vy[i] + (kvy3[i] * dt );
            kz4[i]  = vz[i] + ( kvz3[i] * dt) ;
        }
        for( i=0 ; i<n ; i++ ){
            vx[i] = vx[i] + (dt/6.0)*( kvx1[i] + 2*kvx2[i] + 2*kvx3[i] + kvx4[i] );
            vy[i] = vy[i] + (dt/6.0)*( kvy1[i] + 2*kvy2[i] + 2*kvy3[i] + kvy4[i] );
            vz[i] = vz[i] + (dt/6.0)*( kvz1[i] + 2*kvz2[i] + 2*kvz3[i] + kvz4[i] );
            x[i] = x[i] + (dt/6.0)*( kx1[i] + 2*kx2[i] + 2*kx3[i] + kx4[i] );
            y[i] = y[i] + (dt/6.0)*( ky1[i] + 2*ky2[i] + 2*ky3[i] + ky4[i] );
            z[i] = z[i] + (dt/6.0)*( kz1[i] + 2*kz2[i] + 2*kz3[i] + kz4[i] );
        }
    }
    fclose (pFile1);
    fclose (pFile2);
    //fclose (ifp);
    time_t t2 = clock();
    printf("Time: %f\n",(t2-t1)/(float)CLOCKS_PER_SEC);
    return 0;
}

