#include <iostream>
#include <stdlib.h>
#include <math.h>

using namespace std;

int main(){
    int n;
    cin >> n;
    cout << n << "\n";
    cout << 1 << "\n"; //steps
    cout << 0.001 << "\n"; //dt
    for (int i=0;i<n;i++){
        for (int j=0;j<3;j++)
            cout << i+1 << "\n";
        for (int k=0;k<3;k++)
            cout << i*2*pow(-1,i) << "\n";
        cout << 1;
    }
    return 0;
}
