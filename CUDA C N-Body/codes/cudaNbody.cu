#include<stdio.h>
#include<stdlib.h>
#include<iostream>
#include<cuda.h>

const float G=6.67e-11;
const int threadsPerBlock = 128 * 3 ;
const int blocksPerGrid = 128 ; //must be even

__global__ void cudaAcce(float* dev_pos, float* dev_fb, float* dev_blocksA, int nBodies) {
    __shared__ float cache[threadsPerBlock];
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    int cacheIndex = threadIdx.x;
    int crdIndex = cacheIndex % 3;
    float temp;
    float acc = 0;
    while ( tid < nBodies ) {
        temp = dev_pos[tid] - dev_fb[crdIndex];
        if ( temp < 0.5f )
            acc += (+1 * G)/(temp * temp);
        else if ( temp > 0.5f )
            acc += (-1 * G)/(temp * temp);
        tid += blockDim.x * gridDim.x;
    }
    cache[cacheIndex] = acc;
    __syncthreads();
    int i=blockDim.x/2;
    while (i > 2) {
        if ( cacheIndex<i )
            cache[cacheIndex] += cache[cacheIndex + i];
        __syncthreads();
        i /= 2;
    }
    switch (cacheIndex) {
        case 0:
            dev_blocksA[blockIdx.x * 3 + 0]=cache[0];
            break;
        case 1:
            dev_blocksA[blockIdx.x * 3 + 1]=cache[1];
            break;
        case 2:
            dev_blocksA[blockIdx.x * 3 + 2]=cache[2];
            break;
    }
}

void scanData(float* ptr_pos, float* ptr_vel, float* ptr_mas, int* nBodies) {
    for (int i=0; i<*nBodies; i++)
        for (int crd=0; crd<3; crd++) {
            int pos_cur = i * 3 + crd ;
            scanf("%f", ptr_pos[pos_cur]);
        }
    for (int i=0; i<*nBodies; i++)
        for (int crd=0; crd<3; crd++) {
            int vel_cur = i * 3 + crd ;
            scanf("%f", ptr_vel[vel_cur]);
        }
    for (int i=0; i<*nBodies; i++)
        scanf("%f", ptr_mas[i]);
    printf("scanData completed successfully");
}

void rungeKutta(float* ptr_pos, float* ptr_vel, float* ptr_mas, int* nBodies, float* dt, float* ptr_fb) {
    size_t mainSize = *nBodies * sizeof (float);
    float* rk4_pos = (float*)malloc( 4 * 3 * mainSize );
    float* rk4_vel = (float*)malloc( 4 * 3 * mainSize ); 
    float* dev_pos;
    cudaMalloc( (void**)&dev_pos, 3 * mainSize );
    cudaMemcpy( dev_pos, ptr_pos, 3 * mainSize, cudaMemcpyHostToDevice );
    float* dev_mas;
    cudaMalloc( (void**)&dev_mas, mainSize);
    cudaMemcpy( dev_mas, ptr_mas, mainSize, cudaMemcpyHostToDevice );
    float* dev_fb;
    cudaMalloc( (void**)&dev_fb, 3 * sizeof(float) );	
    cudaMalloc( (void**)&dev_blocksA, 3 * blocksPerGrid * sizeof(float) );
    float* ptr_blocksA = (float*)malloc( 3 * blocksPerGrid * sizeof(float) );
    for (int fbody = 0; fbody < *nBodies; fbody++) {
        int fb_rk_cur = fbody * 12 ;
        int fb_pos_cur = fbody * 3 ;
        for (int i=0; i<3; i++)
            ptr_fb[i] = ptr_pos[ fb_pos_cur + i ];
        cudaMemcpy( dev_fb, ptr_fb, 3 * sizeof(float), cudaMemcpyHostToDevice );
        cudaAcce<<<blocksPerGrid,threadsPerBlock>>>(dev_pos,dev_fb,dev_blocksA,*nBodies);
        cudaMemcpy( ptr_blocksA, dev_blocksA, 3 * blocksPerGrid * sizeof(float), cudaMemcpyDeviceToHost);
        int i = blocksPerGrid * 3 / 2;
        while (i>2) {
            for ( int j=0; j<i; j++ ) {
                ptr_blocksA[j] += ptr_blocksA[j+i];
            }
            i /= 2;
        }

        rk4_vel[fb_rk_cur+0]=ptr_blocksA[0];
        rk4_vel[fb_rk_cur+1]=ptr_blocksA[1];
        rk4_vel[fb_rk_cur+2]=ptr_blocksA[2];

        rk4_pos[fb_rk_cur+0]=ptr_vel[fb_pos_cur+0];
        rk4_pos[fb_rk_cur+1]=ptr_vel[fb_pos_cur+1];
        rk4_pos[fb_rk_cur+2]=ptr_vel[fb_pos_cur+2];

        for (int i=0; i<3; i++)
            ptr_fb[i] = ptr_pos[ fb_pos_cur + i ] + rk4_vel[fb_rk_cur + i]*0.5f*(*dt);
        cudaMemcpy( dev_fb, ptr_fb, 3 * sizeof(float), cudaMemcpyHostToDevice );
        cudaAcce<<<blocksPerGrid,threadsPerBlock>>>(dev_pos,dev_fb,dev_blocksA,*nBodies);
        cudaMemcpy( ptr_blocksA, dev_blocksA, 3 * blocksPerGrid * sizeof(float), cudaMemcpyDeviceToHost);
        i = blocksPerGrid * 3 / 2;
        while (i>2) {
            for ( int j=0; j<i; j++ ) {
                ptr_blocksA[j] += ptr_blocksA[j+i];
            }
            i /= 2;
        }

        rk4_vel[fb_rk_cur+3]=ptr_blocksA[0];
        rk4_vel[fb_rk_cur+4]=ptr_blocksA[1];
        rk4_vel[fb_rk_cur+5]=ptr_blocksA[2];

        rk4_pos[fb_rk_cur+3]=ptr_vel[fb_pos_cur+0]*rk4_vel[fb_rk_cur+0]*(*dt)*0.5f;
        rk4_pos[fb_rk_cur+4]=ptr_vel[fb_pos_cur+1]*rk4_vel[fb_rk_cur+1]*(*dt)*0.5f;
        rk4_pos[fb_rk_cur+5]=ptr_vel[fb_pos_cur+2]*rk4_vel[fb_rk_cur+2]*(*dt)*0.5f;

        for (int i=0; i<3; i++)
            ptr_fb[i] = ptr_pos[ fb_pos_cur + i ] + rk4_vel[fb_rk_cur + i + 3]*0.5f*(*dt);
        cudaMemcpy( dev_fb, ptr_fb, 3 * sizeof(float), cudaMemcpyHostToDevice );
        cudaAcce<<<blocksPerGrid,threadsPerBlock>>>(dev_pos,dev_fb,dev_blocksA,*nBodies);
        cudaMemcpy( ptr_blocksA, dev_blocksA, 3 * blocksPerGrid * sizeof(float), cudaMemcpyDeviceToHost);
        i = blocksPerGrid * 3 / 2;
        while (i>2) {
            for ( int j=0; j<i; j++ ) {
                ptr_blocksA[j] += ptr_blocksA[j+i];
            }
            i /= 2;
        }

        rk4_vel[fb_rk_cur+6]=ptr_blocksA[0];
        rk4_vel[fb_rk_cur+7]=ptr_blocksA[1];
        rk4_vel[fb_rk_cur+8]=ptr_blocksA[2];

        rk4_pos[fb_rk_cur+6]=ptr_vel[fb_pos_cur+0]*rk4_vel[fb_rk_cur+3]*(*dt)*0.5f;
        rk4_pos[fb_rk_cur+7]=ptr_vel[fb_pos_cur+1]*rk4_vel[fb_rk_cur+4]*(*dt)*0.5f;
        rk4_pos[fb_rk_cur+8]=ptr_vel[fb_pos_cur+2]*rk4_vel[fb_rk_cur+5]*(*dt)*0.5f;

        for (int i=0; i<3; i++)
            ptr_fb[i] = ptr_pos[ fb_pos_cur + i ] + rk4_vel[fb_rk_cur + i + 6]*(*dt);
        cudaMemcpy( dev_fb, ptr_fb, 3 * sizeof(float), cudaMemcpyHostToDevice );
        cudaAcce<<<blocksPerGrid,threadsPerBlock>>>(dev_pos,dev_fb,dev_blocksA,*nBodies);
        cudaMemcpy( ptr_blocksA, dev_blocksA, 3 * blocksPerGrid * sizeof(float), cudaMemcpyDeviceToHost);
        i = blocksPerGrid * 3 / 2;
        while (i>2) {
            for ( int j=0; j<i; j++ ) {
                ptr_blocksA[j] += ptr_blocksA[j+i];
            }
            i /= 2;
        }

        rk4_vel[fb_rk_cur+ 9]=ptr_blocksA[0];
        rk4_vel[fb_rk_cur+10]=ptr_blocksA[1];
        rk4_vel[fb_rk_cur+11]=ptr_blocksA[2];

        rk4_pos[fb_rk_cur+ 9]=ptr_vel[fb_pos_cur+0]*rk4_vel[fb_rk_cur+6]*(*dt);
        rk4_pos[fb_rk_cur+10]=ptr_vel[fb_pos_cur+1]*rk4_vel[fb_rk_cur+7]*(*dt);
        rk4_pos[fb_rk_cur+11]=ptr_vel[fb_pos_cur+2]*rk4_vel[fb_rk_cur+8]*(*dt);
    }

}

int main(){
    //FILE pFile;
    int nBodies;
    scanf("%d",&nBodies);
    if ( nBodies > 0 ) {
        nBodies = ( (nBodies + (threadsPerBlock/3) - 1) / (threadsPerBlock/3) ) * (threadsPerBlock/3 );
        printf("Number of bodies %d \n" , nBodies);
    }
    else {
        printf("Err");
        return 0;
    }
    int nSteps;
    scanf("%d", &nSteps);
    float dt;
    scanf("%f", &dt);
    size_t mainSize = nBodies * sizeof(float);
    float* ptr_pos = (float*)malloc(3 * mainSize);
    float* ptr_vel = (float*)malloc(3 * mainSize);
    float* ptr_mas = (float*)malloc(mainSize);
    float* ptr_fb = (float*)malloc(3 * sizeof(float));
    scanData(ptr_pos,ptr_vel,ptr_mas,&nBodies);
    time_t t1 = clock();
    //pFile = fopen("positions.list","w");
    for (int i=0; i<nBodies; i++) {
        rungeKutta(ptr_pos,ptr_vel,ptr_mas,&nBodies,&dt,ptr_fb);
        // missed
    }
    return 0;
}

