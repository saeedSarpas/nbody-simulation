#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <time.h>
using namespace std;
int Read(double* ptr_X,double* ptr_V0,int N_body){
    int coordinate,number;
    cout << endl;
    for (number=0;number<N_body;number++){
        for(coordinate=0;coordinate<3;coordinate++){
            int cursor = 3 * number + coordinate;
            cin >> ptr_X[cursor];
        }
    }
    for (number=0;number<N_body;number++){
        for(coordinate=0;coordinate<3;coordinate++){
            int cursor = 3 * number + coordinate;
            cin >> ptr_V0[cursor];
        }
    }
    return 1;
}
double Acce(int fbody,double f_b_X,int crd,int N_body,double* ptr_X){
    const double G=6.67;
    int sbody;
    double netA=0;
    for (sbody=0;sbody<N_body;sbody++) {
        int s_b_cursor = sbody * 3 + crd;
        double dX = f_b_X - ptr_X[s_b_cursor];
        if(dX<-0.5)
            netA += (+1 * G) / (dX*dX);
        else
            if(dX>0.5)
                netA += (-1 * G) / (dX*dX);
    }
    return (netA);
}
void RungeKuttaAcceleration(double* ptr_X,double* ptr_V0,int N_body,double dt){
    int fbody,crd;
    double* coeff = (double*)malloc(N_body*3*8*sizeof(double));
    for (fbody=0;fbody<N_body;fbody++)
        for (crd=0;crd<3;crd++){
            int f_b_cursor = fbody * 3 + crd;
            int coeff_cursor = fbody * 24 + crd * 8;
            coeff[coeff_cursor+4] = Acce(fbody,ptr_X[f_b_cursor],crd,N_body,ptr_X);
            coeff[coeff_cursor+0] = ptr_V0[f_b_cursor];
            coeff[coeff_cursor+5] = Acce(fbody,ptr_X[f_b_cursor]+coeff[coeff_cursor+0]*0.5*dt,crd,N_body,ptr_X);
            coeff[coeff_cursor+1] = ptr_V0[f_b_cursor]*coeff[coeff_cursor+4]*dt*0.5;
            coeff[coeff_cursor+6] = Acce(fbody,ptr_X[f_b_cursor]+coeff[coeff_cursor+1]*0.5*dt,crd,N_body,ptr_X);
            coeff[coeff_cursor+2] = ptr_V0[f_b_cursor]*coeff[coeff_cursor+5]*dt*0.5;
            coeff[coeff_cursor+7] = Acce(fbody,ptr_X[f_b_cursor]+coeff[coeff_cursor+2]*dt,crd,N_body,ptr_X);
            coeff[coeff_cursor+3] = ptr_V0[f_b_cursor]*coeff[coeff_cursor+6]*dt;
        }
    for (fbody=0;fbody<N_body;fbody++)
        for(crd=0;crd<3;crd++) {
            int f_b_cursor = fbody * 3 + crd;
            int coeff_cursor = fbody * 24 + crd * 8;
            ptr_V0[f_b_cursor] += (coeff[coeff_cursor+4]+2*coeff[coeff_cursor+5]+2*coeff[coeff_cursor+6]+coeff[coeff_cursor+7])*dt/6;
            ptr_X[f_b_cursor] += (coeff[coeff_cursor+0]+2*coeff[coeff_cursor+1]+2*coeff[coeff_cursor+2]+coeff[coeff_cursor+3])*dt/6;
        }
    free(coeff);
}
int main(){
    FILE * pFile;
    int N_body,N=200000;
    scanf ("%d \n",&N_body);
    size_t twodim = 3 * N_body * sizeof(double);
    double* ptr_X = (double*)malloc(twodim);
    double* ptr_V0 = (double*)malloc(twodim);
    double dt=0.001;
    int i,coordinate,number;
    Read(ptr_X,ptr_V0,N_body);
    time_t t1 = clock();
    pFile = fopen ("Locations.txt","w");
    for (i=0;i<N;i++){
        RungeKuttaAcceleration(ptr_X,ptr_V0,N_body,dt);
        fprintf (pFile, "  ");
        for (number=0;number<N_body;number++){
            int crsr = number * 3;
            fprintf (pFile, "%+f|%+f|%+f|  ",ptr_X[crsr],ptr_X[crsr+1],ptr_X[crsr+2]);
        }
        fprintf (pFile, "\n");
    }
    fclose (pFile);
    free(ptr_V0);
    free(ptr_X);
    time_t t2 = clock();
    cout << (t2-t1)/(double)CLOCKS_PER_SEC << endl ;
    return 0;
}

