if [ -f SpeedTest.data ]
then
    rm SpeedTest.data
fi
for (( i=2; i <= $1; i++ ))
do
    #echo "Running $i"
    printf "Running %3d of %3d \n" "$i" "$1"
    if [ -f Locations.txt ]
    then
        rm Locations.txt
    fi
    echo $i >> SpeedTest.data 
    echo $i | ./input.out | tee $i.txt | ./nbody.out >> SpeedTest.data
done
