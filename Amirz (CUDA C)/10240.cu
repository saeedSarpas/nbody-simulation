#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <time.h>

// NBODY.cu Amir Najafgholi
// Version 1.0
// 6 may 2012

int stpn ;
float m;
//define G 6.67

const int n = 4096;
const int threadsPerBlock = 128;
const int blocksPerGrid = 128;

__global__ void add(float *a , float *b, float t , float *c) {
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    if ( tid < n )  c[tid] = a[tid] + t*b[tid];
}

__global__ void a_gpu(int state, float x , float y , float z , float * xprim , float * yprim, float * zprim, float * chm ,float *c) {
    __shared__ float cache[threadsPerBlock];
    int tid = threadIdx.x + blockIdx.x * blockDim.x;
    int cacheIndex = threadIdx.x;
    float r , xzegond , yzegond , zzegond , zarib ;
    float aprim;
    float temp = 0.0f;
    aprim = 0.0f;
    while (tid < n) {
        xzegond = x - xprim[tid];
        yzegond = y - yprim[tid];
        zzegond = z - zprim[tid];
        r = sqrt( xzegond*xzegond + yzegond*yzegond + zzegond*zzegond );
        if( state == 1 ) zarib = (xzegond / r);
        if( state == 2 ) zarib = (yzegond / r);
        if( state == 3 ) zarib = (zzegond / r);
        if ( (r > 0.001f) ){
            aprim =  zarib*chm[tid]/( r*r );
            temp += aprim;
        }
        tid += blockDim.x * gridDim.x;
    }

    // set the cache values
    cache[cacheIndex] = temp;

    // synchronize threads in this block
    __syncthreads();

    // for reductions, threadsPerBlock must be a power of 2
    // because of the following code

    int i = blockDim.x/2;
    while (i != 0) {
        if (cacheIndex < i)
            cache[cacheIndex] += cache[cacheIndex + i];
        __syncthreads();
        i /= 2;
    }
    if (cacheIndex == 0) c[blockIdx.x] = cache[0];
}

void a(float x , float y , float z ,float* xprim , float* yprim , float* zprim , float* chm , float* input ,float* d_xprim , float* d_yprim , float* d_zprim , float * d_chm ,float* d_partial_c ){
    float * partial_c ;
    partial_c = (float*)malloc( blocksPerGrid*sizeof(float) );
    a_gpu<<<blocksPerGrid,threadsPerBlock>>>( 1 , x , y , z , d_xprim , d_yprim , d_zprim , d_chm , d_partial_c);
    cudaMemcpy( partial_c , d_partial_c , blocksPerGrid*sizeof(float) , cudaMemcpyDeviceToHost );
    input[0] = 0.f;
    for (int i=0; i<blocksPerGrid; i++)
        input[0] += partial_c[i];
    a_gpu<<<blocksPerGrid,threadsPerBlock>>>( 2 , x , y , z , d_xprim , d_yprim , d_zprim , d_chm , d_partial_c);
    cudaMemcpy( partial_c , d_partial_c , blocksPerGrid*sizeof(float) , cudaMemcpyDeviceToHost );
    input[1] = 0.f;
    for (int i=0; i<blocksPerGrid; i++) {
        input[1] += partial_c[i];
    }
    a_gpu<<<blocksPerGrid,threadsPerBlock>>>( 3 , x , y , z , d_xprim , d_yprim , d_zprim , d_chm , d_partial_c);
    cudaMemcpy( partial_c , d_partial_c , blocksPerGrid*sizeof(float) , cudaMemcpyDeviceToHost );
    input[2] = 0.f;
    for (int i=0; i<blocksPerGrid; i++) {
        input[2] += partial_c[i];
    }
}

int main( ){
    //cudaSetDevice(0);
    const float G = 1;
    FILE *pFile1 , *pFile2;
    //FILE *ifp;
    extern int stpn;
    extern float m;
    int  i,k,j,kk;
    int counter;
    float dt;
    float mass;
    float sign,dice;
    //string FileName="Output1.dat";
    pFile1 = fopen ("Output1.dat","w");
    pFile2 = fopen ("Output2.dat","w");
    //printf("Number of Bodies,Steps,Time Interval,Mass Unit,Snapshot Time Intervals\n");
    //cin>> n >> stpn >> dt >> mass>> kk;
    mass = 1;
    stpn = 100000;
    dt=0.001;
    kk = 1000;
    size_t dim = n * sizeof(float);
    float* acc  = (float*)malloc( 3*sizeof(float) );
    float* x    = (float*)malloc(dim);  float* y    = (float*)malloc(dim);  float* z    = (float*)malloc(dim);
    float* xp   = (float*)malloc(dim);  float* yp   = (float*)malloc(dim);  float* zp   = (float*)malloc(dim);
    float* vx   = (float*)malloc(dim);  float* vy   = (float*)malloc(dim);  float* vz   = (float*)malloc(dim);
    float* kx1  = (float*)malloc(dim);  float* ky1  = (float*)malloc(dim);  float* kz1  = (float*)malloc(dim);
    float* kx2  = (float*)malloc(dim);  float* ky2  = (float*)malloc(dim);  float* kz2  = (float*)malloc(dim);
    float* kx3  = (float*)malloc(dim);  float* ky3  = (float*)malloc(dim);  float* kz3  = (float*)malloc(dim);
    float* kx4  = (float*)malloc(dim);  float* ky4  = (float*)malloc(dim);  float* kz4  = (float*)malloc(dim);
    float* kvx1 = (float*)malloc(dim);  float* kvy1 = (float*)malloc(dim);  float* kvz1 = (float*)malloc(dim);
    float* kvx2 = (float*)malloc(dim);  float* kvy2 = (float*)malloc(dim);  float* kvz2 = (float*)malloc(dim);
    float* kvx3 = (float*)malloc(dim);  float* kvy3 = (float*)malloc(dim);  float* kvz3 = (float*)malloc(dim);
    float* kvx4 = (float*)malloc(dim);  float* kvy4 = (float*)malloc(dim);  float* kvz4 = (float*)malloc(dim);
    float* chm  = (float*)malloc(dim);
    //float  c1;
    float * d_xprim , * d_yprim , * d_zprim , * d_chm;
    float * d_partial_c;
    //allocate the memory on the GPU
    cudaMalloc( (void**)&d_xprim , n*sizeof(float) );
    cudaMalloc( (void**)&d_yprim , n*sizeof(float) );
    cudaMalloc( (void**)&d_zprim , n*sizeof(float) );
    cudaMalloc( (void**)&d_chm   , n*sizeof(float) );
    cudaMalloc( (void**)&d_partial_c , blocksPerGrid*sizeof(float) );
    //baraie mohasebeie for haie rk4
    float * d_a , * d_b , * d_c , * d_av , * d_bv , * d_cv , * d_k , * dev_c ;
    cudaMalloc( (void**)&d_a , n*sizeof(float) );
    cudaMalloc( (void**)&d_b , n*sizeof(float) );
    cudaMalloc( (void**)&d_c , n*sizeof(float) );
    cudaMalloc( (void**)&d_av , n*sizeof(float) );
    cudaMalloc( (void**)&d_bv , n*sizeof(float) );
    cudaMalloc( (void**)&d_cv , n*sizeof(float) );
    cudaMalloc( (void**)&d_k , n*sizeof(float) );
    cudaMalloc( (void**)&dev_c , n*sizeof(float) );
    m=-1*G * mass;
//    ifp = fopen ("10240.dat","r");
//    for (i=0 ; i<n ; i++ ){
//        fscanf (ifp, "%f	%f	%f	%f	%f	%f	%f	" ,&c1, &x[i], &y[i],&z[i], &vx[i], &vy[i], &vz[i]);
//        chm[i]=c1*m;
//    }
    for ( i=0; i<n ; i++ ){

        // taeen X
        dice = ((float) rand())/RAND_MAX;
        if ( dice<= 0.5 )
            sign = 1;
        else
            sign =-1;
        x[i]  = sign*((float) rand())*50/RAND_MAX;

        // taeen Y
        dice = ((float) rand())/RAND_MAX;
        if ( dice<= 0.5 )
            sign = 1;
        else
            sign =-1;
        y[i]  = sign*((float) rand())*50/RAND_MAX;

        // taeen Z
        dice = ((float) rand())/RAND_MAX;
        if ( dice<= 0.5 )
            sign = 1;
        else
            sign =-1;
        z[i]  = sign*((float) rand())*50/RAND_MAX;

        // taeen VX
        dice = ((float) rand())/RAND_MAX;
        if ( dice<= 0.5 )
            sign = 1;
        else
            sign =-1;
        vx[i] = sign*((float) rand())*0.01/RAND_MAX;

        // taeen VY
        dice = ((float) rand())/RAND_MAX;
        if ( dice<= 0.5 )
            sign = 1;
        else
            sign =-1;
        vy[i] = sign*((float) rand())*0.01/RAND_MAX;

        // taeen VZ
        dice = ((float) rand())/RAND_MAX;
        if ( dice<= 0.5 )
            sign = 1;
        else
            sign =-1;
        vz[i] = sign*((float) rand())*0.01/RAND_MAX;

        //taeen Jerm
        chm[i]=0.01*m;
    }
//    for ( i=0; i<n ; i++ ){
//        x[i]   =  i+1;
//        y[i]   =  i+1;
//        z[i]   =  i+1;
//        vx[i]  = i*2*pow(-1,i);
//        vy[i]  = i*2*pow(-1,i);
//        vz[i]  = i*2*pow(-1,i);
//        chm[i] = 0.01*m;
//    }
//
//    x[0]   = 0.0    ;
//    y[0]   = 0.0    ;
//    z[0]   = 0.0    ;
//    vx[0]  = 0.0    ;
//    vy[0]  = 0.0    ;
//    vz[0]  = 0.0    ;
//    chm[0] = 1000*m ;
//
//    x[1]   = 10.0   ;
//    y[1]   = 0.0    ;
//    z[1]   = 0.0    ;
//    vx[1]  = 0.0    ;
//    vy[1]  = 10.0   ;
//    vz[1]  = 0.0    ;
//    chm[1] = 0.01*m ;
//
//    x[2]    = -20.0  ;
//    y[2]    =  0.0   ;
//    z[2]    =  0.0   ;
//    vx[2]   =  0.0   ;
//    vy[2]   = -2.5   ;
//    vz[2]   =  0.0   ;
//    chm[2]  =  0.01*m;
    time_t t1 = clock();
    counter = 0;
    for( k=0 ; k<=stpn ; k++){
        counter += 1;
        if (counter%1 == 0) printf("%d", counter);
        //printf ("%d ", k);
        //printf ("\n");
        if (k%kk == 0) {
            for( j=0 ; j<n ; j++  ){
                fprintf (pFile1, "%f	%f	%f	" ,x[j],y[j],z[j]);
                fprintf (pFile2, "%f	%f	%f	" ,vx[j],vy[j],vz[j]);
            }
            fprintf (pFile1, "\n");
            fprintf (pFile2, "\n");
        }

        // maghadir ra baraie mohasebeie shetab ha rooie gpu mifrestad
        cudaMemcpy( d_xprim , x , n*sizeof(float),cudaMemcpyHostToDevice );
        cudaMemcpy( d_yprim , y , n*sizeof(float),cudaMemcpyHostToDevice );
        cudaMemcpy( d_zprim , z , n*sizeof(float),cudaMemcpyHostToDevice );
        cudaMemcpy( d_chm   , chm   , n*sizeof(float),cudaMemcpyHostToDevice );
        for( i=0 ; i < n ; i++) {
            a( x[i] , y[i] , z[i] , x , y , z, chm , acc ,d_xprim , d_yprim , d_zprim , d_chm , d_partial_c );
            kvx1[i] = acc[0];
            kvy1[i] = acc[1];
            kvz1[i] = acc[2];
            kx1[i]  = vx[i];
            ky1[i]  = vy[i];
            kz1[i]  = vz[i];
        }

        // maghadir ra baraie mohasebeie  barkhi zaraiebe rk4
        cudaMemcpy( d_a , x , n*sizeof(float),cudaMemcpyHostToDevice );
        cudaMemcpy( d_b , y , n*sizeof(float),cudaMemcpyHostToDevice );
        cudaMemcpy( d_c , z , n*sizeof(float),cudaMemcpyHostToDevice );
        cudaMemcpy( d_av , vx , n*sizeof(float),cudaMemcpyHostToDevice );
        cudaMemcpy( d_bv , vy , n*sizeof(float),cudaMemcpyHostToDevice );
        cudaMemcpy( d_cv , vz , n*sizeof(float),cudaMemcpyHostToDevice );

        //1 Rk4
        cudaMemcpy( d_k , kx1 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_a , d_k ,dt/2, dev_c );
        cudaMemcpy( xp, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );
        cudaMemcpy( d_k , ky1 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_b , d_k ,dt/2, dev_c );
        cudaMemcpy( yp, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );
        cudaMemcpy( d_k , kz1 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_c , d_k ,dt/2, dev_c );
        cudaMemcpy( zp, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );

        // maghadir ra baraie mohasebeie shetab ha rooie gpu mifrestad
        cudaMemcpy( d_xprim , x , n*sizeof(float),cudaMemcpyHostToDevice );
        cudaMemcpy( d_yprim , y , n*sizeof(float),cudaMemcpyHostToDevice );
        cudaMemcpy( d_zprim , z , n*sizeof(float),cudaMemcpyHostToDevice );
        cudaMemcpy( d_chm  , chm, n*sizeof(float),cudaMemcpyHostToDevice );
        for( i=0 ; i < n ; i++) {
            a( x[i] , y[i] , z[i] , x , y , z, chm , acc  , d_xprim , d_yprim , d_zprim , d_chm ,d_partial_c );
            kvx2[i] = acc[0];
            kvy2[i] = acc[1];
            kvz2[i] = acc[2];
        }

        //2 Rk4
        cudaMemcpy( d_k , kvx1 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_av , d_k ,dt/2, dev_c );
        cudaMemcpy( kx2, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );
        cudaMemcpy( d_k , kvy1 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_bv , d_k ,dt/2, dev_c );
        cudaMemcpy( ky2, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );
        cudaMemcpy( d_k , kvz1 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_cv , d_k ,dt/2, dev_c );
        cudaMemcpy( kz2, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );

        //3 Rk4
        cudaMemcpy( d_k , kx2 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_a , d_k ,dt/2, dev_c );
        cudaMemcpy( xp, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );
        cudaMemcpy( d_k , ky2 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_b , d_k ,dt/2, dev_c );
        cudaMemcpy( yp, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );
        cudaMemcpy( d_k , kz2 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_c , d_k ,dt/2, dev_c );
        cudaMemcpy( zp, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );

        //maghadir ra baraie mohasebeie shetab ha rooie gpu mifrestad
        cudaMemcpy( d_xprim , x , n*sizeof(float),cudaMemcpyHostToDevice );
        cudaMemcpy( d_yprim , y , n*sizeof(float),cudaMemcpyHostToDevice );
        cudaMemcpy( d_zprim , z , n*sizeof(float),cudaMemcpyHostToDevice );
        cudaMemcpy( d_chm   , chm   , n*sizeof(float),cudaMemcpyHostToDevice );
        for( i=0 ; i < n ; i++) {
            a( x[i] , y[i] , z[i] , x , y , z, chm , acc , d_xprim , d_yprim , d_zprim , d_chm , d_partial_c );
            kvx3[i] = acc[0];
            kvy3[i] = acc[1];
            kvz3[i] = acc[2];
        }

        //4 Rk4
        cudaMemcpy( d_k , kvx2 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_av , d_k ,dt/2, dev_c );
        cudaMemcpy( kx3, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );
        cudaMemcpy( d_k , kvy2 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_bv , d_k ,dt/2, dev_c );
        cudaMemcpy( ky3, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );
        cudaMemcpy( d_k , kvz2 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_cv , d_k ,dt/2, dev_c );
        cudaMemcpy( kz3, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );

        //5   Rk4
        cudaMemcpy( d_k , kx3 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_a , d_k ,dt/2, dev_c );
        cudaMemcpy( xp, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );
        cudaMemcpy( d_k , ky3 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_b , d_k ,dt/2, dev_c );
        cudaMemcpy( yp, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );
        cudaMemcpy( d_k , kz3 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_c , d_k ,dt/2, dev_c );
        cudaMemcpy( zp, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );

        // maghadir ra baraie mohasebeie shetab ha rooie gpu mifrestad
        cudaMemcpy( d_xprim , x , n*sizeof(float),cudaMemcpyHostToDevice );
        cudaMemcpy( d_yprim , y , n*sizeof(float),cudaMemcpyHostToDevice );
        cudaMemcpy( d_zprim , z , n*sizeof(float),cudaMemcpyHostToDevice );
        cudaMemcpy( d_chm   , chm   , n*sizeof(float),cudaMemcpyHostToDevice );
        for( i=0 ; i < n ; i++) {
            a( x[i] , y[i] , z[i] , x , y , z, chm , acc ,d_xprim , d_yprim , d_zprim , d_chm , d_partial_c );
            kvx4[i] = acc[0];
            kvy4[i] = acc[1];
            kvz4[i] = acc[2];
        }

        //6 Rk4
        cudaMemcpy( d_k , kvx3 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_av , d_k ,dt, dev_c );
        cudaMemcpy( kx4, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );
        cudaMemcpy( d_k , kvy3 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_bv , d_k ,dt, dev_c );
        cudaMemcpy( ky4, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );
        cudaMemcpy( d_k , kvz3 , n*sizeof(float),cudaMemcpyHostToDevice );
        add<<<blocksPerGrid,threadsPerBlock>>>( d_cv , d_k ,dt, dev_c );
        cudaMemcpy( kz4, dev_c ,n*sizeof(float), cudaMemcpyDeviceToHost );
        for( i=0 ; i<n ; i++ ){
            vx[i] = vx[i] + (dt/6.0)*( kvx1[i] + 2*kvx2[i] + 2*kvx3[i] + kvx4[i] );
            vy[i] = vy[i] + (dt/6.0)*( kvy1[i] + 2*kvy2[i] + 2*kvy3[i] + kvy4[i] );
            vz[i] = vz[i] + (dt/6.0)*( kvz1[i] + 2*kvz2[i] + 2*kvz3[i] + kvz4[i] );
            x[i] = x[i] + (dt/6.0)*( kx1[i] + 2*kx2[i] + 2*kx3[i] + kx4[i] );
            y[i] = y[i] + (dt/6.0)*( ky1[i] + 2*ky2[i] + 2*ky3[i] + ky4[i] );
            z[i] = z[i] + (dt/6.0)*( kz1[i] + 2*kz2[i] + 2*kz3[i] + kz4[i] );
        }
    }
    cudaFree( d_a  );
    cudaFree( d_b  );
    cudaFree( d_c  );
    cudaFree( d_av );
    cudaFree( d_bv );
    cudaFree( d_cv );
    cudaFree( d_k  );
    cudaFree( dev_c );
    cudaFree( d_xprim );
    cudaFree( d_yprim );
    cudaFree( d_zprim );
    cudaFree( d_chm );
    cudaFree( d_partial_c );
    fclose (pFile1);
    fclose (pFile2);
    //fclose (ifp);
    time_t t2 = clock();
    printf("Time: %f\n",(t2-t1)/(float)CLOCKS_PER_SEC);
    return 0;
}

